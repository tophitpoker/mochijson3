%%%---------------------------------------------------------------------------------------
%%% File    : jsonh.erl
%%% Author  : Max Burinov <bourinov@gmail.com>
%%% Description : JSON Helper. Designed to provide complimentary functions for mochijson library.
%%%
%%% Created :  09.01.2012
%%%---------------------------------------------------------------------------------------
-module(mochijson3_helper).
-author('bourinov@gmail.com').

-export([get_path_value/2]).

%% @doc Find value from mochijson3 output
%% @end
-spec get_path_value(Request :: [{pos_integer(), binary()}] | [], 
                        Data :: [{atom(), [{binary(), binary()}, ...]}] | {atom(), [{binary(), binary()}]}) -> 
                                                                                        binary() | integer() | [integer()] 
                                                                                        | [binary()]| error.
get_path_value([], Data) ->
    % if we have no [], we return Data
    Data;

%% @doc Find value from mochijson3 output
%% @end         
get_path_value(Path, RD) when is_list(RD) ->
    % Check request data length
    case length(Path) of
        1 ->
            % Get current request body
            [{Num, NeedToFind}] = Path,
            % Find in the data structure
            FindedData = lists:nth(Num, RD),
            % Get finded data
            {MaybeStruct, NeedToFindValue} = FindedData,
            case MaybeStruct of
                struct ->
                    % Get value
                    TryGetValue = lists:keyfind(NeedToFind, 1, NeedToFindValue),
                    case TryGetValue of
                        false ->
                            error;
                        _ ->
                            {_, Value} = TryGetValue,
                            Value
                    end;
                    _ ->
                        MaybeStruct
           end;
        _ ->
           case RD of
               [] ->
                   error;
               _ ->
                   % Find further
                   get_path_value(Path, {struct, RD})
           end
    end;

%% @doc Find value from mochijson3 output
%% @end
get_path_value(Path, RD) when is_tuple(RD) ->
   % Get data struct body
   {struct, DataList} = RD,
   % Get first request body
   [H | T] = Path,
   % Find value
   {Num, NeedFindValue} = H, 
     
   List = lists:filter(fun(X) -> 
              {Value, JSonValue} = X,
              if
                  Value == struct ->
                     TryToFindNext = lists:keyfind(NeedFindValue, 1, JSonValue),
                     case TryToFindNext of
                        false ->
                            false;
                        _ ->
                            true  
                     end;   
                  true ->
                     Value == NeedFindValue
              end
          end,
          DataList),
      
   case List of
      [] ->
         error;
      _ ->
          if
              Num > length(List) ->
                  error;
              true ->
                  % Return value
                  FindedValue = lists:nth(Num, List),
                  {IsStruct, OtherValue} = FindedValue,
                  case IsStruct of
                      struct ->
                          {struct, FindNextValue} = FindedValue,
                          TryToFindNextValue = lists:keyfind(NeedFindValue, 1, FindNextValue),
                          case TryToFindNextValue of
                              false ->
                                  error;
                              _ ->
                                  {_, Value} = TryToFindNextValue,
                                  get_path_value(T, Value)  
                          end;
                      _ ->
                          get_path_value(T, OtherValue)
                  end
         end
   end.

%% =======================================================================================
%% unit tests
%% =======================================================================================
%% Tests disabled until they can be prevented from running when included
%% as a dependency.
-ifdef(TEST).
-include_lib("eunit/include/eunit.hrl").

get_path_value_test() ->
    Data1 = <<"{\"resp1\":\"resp6\", \"resp2\":\"resp3\", \"resp3\":\"resp5\"}">>,
    Body1 = mochijson3:decode(Data1),
    Result1 = get_path_value([{1, <<"resp2">>}], Body1),
    ?assertEqual(<<"resp3">>, Result1),
    
    Data2 = <<"{\"resp1\":\"resp6\", \"resp2\":\"resp3\", \"resp3\":\"aaaaaaaaaaaaaa\"}">>,
    Body2 = mochijson3:decode(Data2),
    Result2 = get_path_value([{1, <<"resp3">>}], Body2),
    
    ?assertEqual(<<"aaaaaaaaaaaaaa">>, Result2),
    
    Data3 = <<"{\"1\":\"2\", \"response\":[{\"uid\":1103656,\"first_name\":\"Max\",\"last_name\":\"Bourinov\",\"response2\":[{\"uid\":1103656,\"first_name\":\"Max\",\"last_name\":\"Bourinov\"},{\"uid\":1103657,\"first_name\":\"Sasha\",\"last_name\":\"Kuleshov\"}]}]}">>,
    
    Body3 = mochijson3:decode(Data3),
    Result3 = get_path_value([{1, <<"response">>}, {1, <<"response2">>}, {1, <<"first_name">>}], Body3),
    ?assertEqual(<<"Max">>, Result3),
    
    Data4 = <<"{\"resp1\":\"resp6\", \"resp2\":\"resp3\", \"resp3\":\"aaaaaaaaaaaaaa\"}">>,
    Body4 = mochijson3:decode(Data4),
    Result4 = get_path_value([{1, <<"response">>}], Body4),
    ?assertEqual(error, Result4),
    
    Data5 = <<"{\"1\":\"2\", \"response\":[{\"uid\":1103656,\"first_name\":\"Max\",\"last_name\":\"Bourinov\",\"response2\":[{\"uid\":1103656,\"first_name\":\"Max\",\"last_name\":\"Bourinov\"},{\"uid\":1103657,\"first_name\":\"Sasha\",\"last_name\":\"Kuleshov\"}]}]}">>,
    Body5 = mochijson3:decode(Data5),
    Result5 = get_path_value([{1, <<"request">>}, {2, <<"response">>}], Body5),
    ?assertEqual(error, Result5),
    
    Data6 = <<"{\"1\":\"2\", \"response\":[{\"uid\":1103656,\"first_name\":\"Max\",\"last_name\":\"Bourinov\",\"response2\":[{\"uid\":1103656,\"first_name\":\"Max\",\"last_name\":\"Bourinov\"},{\"uid\":1103657,\"first_name\":\"Sasha\",\"last_name\":\"Kuleshov\"}]}]}">>,
    Body6 = mochijson3:decode(Data6),
    Result6 = get_path_value([{1, <<"response">>}, {1, <<"uid">>}], Body6),
    ?assertEqual(1103656, Result6),
    
    Data7 = <<"{
                 \"glossary\": {
                     \"title\":\"example glossary\",
		             \"GlossDiv\": {
                         \"title\": \"S\",
			             \"GlossList\": {
                         \"GlossEntry\": {
                         \"ID\": \"SGML\",
					     \"SortAs\": \"SGML\",
					     \"GlossTerm\": \"Standard Generalized Markup Language\",
					     \"Acronym\": \"SGML\",
					     \"Abbrev\": \"ISO 8879:1986\",
					     \"GlossDef\": {
                         \"para\": \"A meta-markup language, used to create markup languages such as DocBook.\",
						\"GlossSeeAlso\": [\"GML\", \"XML\"]
                    },
					\"GlossSee\": \"markup\"}}}}}">>,
	Body7 = mochijson3:decode(Data7),
	Result7 = get_path_value([{1, <<"glossary">>}, {1, <<"GlossDiv">>}, {1, <<"title">>}], Body7),
	?assertEqual(<<"S">>, Result7),
     
    Data8 = <<"{
                 \"glossary\": {
                     \"title\":\"example glossary\",
		             \"GlossDiv\": {
                         \"title\": \"S\",
			             \"GlossList\": {
                         \"GlossEntry\": {
                         \"ID\": \"SGML\",
					     \"SortAs\": \"SGML\",
					     \"GlossTerm\": \"Standard Generalized Markup Language\",
					     \"Acronym\": \"SGML\",
					     \"Abbrev\": \"ISO 8879:1986\",
					     \"GlossDef\": {
                         \"para\": \"A meta-markup language, used to create markup languages such as DocBook.\",
						\"GlossSeeAlso\": [\"GML\", \"XML\"]
                    },
					\"GlossSee\": \"markup\"}}}}}">>,
					
	Body8 = mochijson3:decode(Data8),
	Result8 = get_path_value([{1, <<"glossary">>}, {1, <<"GlossDiv">>}, {1, <<"titles">>}], Body8),
	
    ?assertEqual(error, Result8),
        
    Data9 = <<"{\"content\":{\"status\":\"placed\",\"order_id\":240753069327930,\"method\":\"payments_status_update\"}}">>,
    Body9 = mochijson3:decode(Data9),
    Result9 = get_path_value([{1, <<"content">>}, {1, <<"order_id">>}], Body9),
	            
    ?assertEqual(240753069327930, Result9),
    
    %% Test list of binary
    Data10 = <<"{\"a\":[\"ASD\",\"bdf\"]}">>,
    Body10 = mochijson3:decode(Data10),
    Result10 = mochijson3_helper:get_path_value([{1, <<"a">>}], Body10),
    ?assertEqual([<<"ASD">>, <<"bdf">>], Result10),
    
    % Test list of integers
    Data11 = <<"{\"a\":[1,2,3,4,5,6,0]}">>,
    Body11 = mochijson3:decode(Data11),
    Result11 = mochijson3_helper:get_path_value([{1, <<"a">>}], Body11),
    ?assertEqual([1,2,3,4,5,6,0], Result11),
    
    ok.
    
-endif.
